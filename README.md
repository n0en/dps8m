<!-- vim: set nocp ft=markdown ts=2 sw=2 ai cc=80 et nolist wrap lbr :-->
<!-- SPDX-License-Identifier: FSFAP -->
<!-- scspell-id: 234ac36d-f778-11ec-8863-80ee73e9b8e7 -->
<!-- Copyright (c) 2006-2022 The DPS8M Development Team
     Copying and distribution of this file, with or without modification,
     are permitted in any medium without royalty provided the copyright
     notice and this notice are preserved.  This file is offered "AS-IS",
     without any warranty. -->

# DPS8M Simulator

[![Pipelines](https://gitlab.com/dps8m/dps8m/badges/master/pipeline.svg?ignore_skipped=true)](https://gitlab.com/dps8m/dps8m/pipelines/latest/)
&nbsp;
[![Best Practices](https://bestpractices.coreinfrastructure.org/projects/6229/badge.svg)](https://bestpractices.coreinfrastructure.org/projects/6229)
&nbsp;
[![Website](https://img.shields.io/website?up_message=online&url=https%3A%2F%2Fdps8m.gitlab.io)](https://dps8m.gitlab.io/)
&nbsp;

 - **DPS8M** is a simulator of the **36‑bit** GE Large Systems / Honeywell /
   Bull 600/6000‑series mainframe computers (Honeywell 6180, Honeywell
   Series‑60 ∕ Level‑68, and Honeywell ∕ Bull **DPS‑8/M**) descended from
   the **GE‑645** and engineered to support the
   [**Multics** operating system](https://swenson.org/multics_wiki/).

 - Visit the
   [**DPS8M Simulator Homepage**](https://dps8m.gitlab.io/)
   for additional information.

## SAST Tools

* [PVS-Studio](https://pvs-studio.com/en/pvs-studio/?utm_source=github&utm_medium=organic&utm_campaign=open_source) - static analyzer for C, C++, C#, and Java code.
